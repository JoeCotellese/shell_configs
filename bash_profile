# This file is executed for login shells either via local 
# or remotely. This includes when a new terminal window is started on OS X


# Setup my PATHS

# default paths
PATH=/bin:/sbin:/usr/bin:/usr/sbin

# include /usr/local to pickup brew installed apps
PATH=/usr/local/bin:/usr/local/sbin:$PATH

# I need this to override the system PHP 
PATH="/usr/local/opt/php@7.0/bin:$PATH"
PATH="/usr/local/opt/php@7.0/sbin:$PATH"

# Android Development
ANDROID_HOME=${HOME}/Library/Android/sdk
PATH="${ANDROID_HOME}/tools/bin:${PATH}"
export ANDROID_HOME PATH

export PATH

# setup the OS X virtual env
# note this depends on the virtualwrapper that is installed with ports
export WORKON_HOME=$HOME/virtualenvs
export PROJECT_HOME=$HOME/git

# Specify Homebrew Cask Options
export HOMEBREW_CASK_OPTS="--appdir=/Applications "

# Setup PYENV
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"

# [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

# Setup PYENV autocomplete per the instructions at https://github.com/pyenv/pyenv
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

# we want to include our aliases in bashrc so
# source the file here.
if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi

# setup HISTORY buffers
HISTSIZE=1000
HISTFILESIZE=2000
